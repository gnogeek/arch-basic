#!/bin/bash
mkswap /dev/sda5
swapon /dev/sda5
mkfs.ext4 -F /dev/sda4
mount /dev/sda4 /mnt
pacstrap /mnt base base-devel nano  --noconfirm
mkdir /mnt/boot/efi
mount /dev/sda1 /mnt/boot/efi
genfstab -p -U /mnt >> /mnt/etc/fstab
DISK_UUID=$(lsblk /dev/sda4 -o partuuid -n)
cat <<EOF > /mnt/root/refind_linux.conf
"Boot using default options" "root=PARTUUID=$DISK_UUID rw add_efi_memmap initrd=boot\intel-ucode.img initrd=boot\initramfs-linux.img"
EOF
#cp Archchroot.bash /mnt/Archchroot.bash
cat <<EOF > /mnt/root/part2.sh
# stuff here to do inside the chroot
# ...
#!/bin/bash
echo en_US.UTF-8 UTF-8 >> /etc/locale.gen
ln -sf /usr/share/zoneinfo/America/Santo_Domingo /etc/localtime
pacman -Sy reflector --noconfirm
reflector --verbose --protocol http --latest 15 --sort rate --save /etc/pacman.d/mirrorlist
pacman -Syy
pacman -S wpa_supplicant refind nano intel-ucode xf86-video-intel wireless_tools nm-connection-editor network-manager-applet pavucontrol pulseaudio alsa-utils git linux linux-headers linux-firmware vi networkmanager  openssh bluez bluez-utils blueman tlp tlp-rdw powertop acpi acpi_call dialog --noconfirm
refind-install
hwclock --systohc
tee -a /etc/hosts <<EOF
127.0.0.1 localhost
::1       localhost
127.0.0.1 Y260
EOF
echo Y260 > /etc/hostname
pacman -Sy  --noconfirm
systemctl enable NetworkManager
systemctl enable sshd
systemctl enable bluetooth
systemctl enable tlp
systemctl enable tlp-sleep
systemctl enable fstrim.timer

systemctl mask systemd-rfkill.service
systemctl mask systemd-rfkill.socket
cp /root/refind_linux.conf /boot/refind_linux.conf
sed -i "s/#Color/Color/" /etc/pacman.conf
sed -i '/Color/a ILoveCandy' /etc/pacman.conf
useradd -m gerlin
echo gerlin:Gnh921014** | chpasswd
echo "gerlin ALL=(ALL) ALL" >> /etc/sudoers.d/gerlin
echo root:Gnh921014** | chpasswd
# ...
exit # to leave the chroot
EOF
arch-chroot /mnt /root/part2.sh

#DISK_UUID=$(lsblk /dev/sda4 -o partuuid -n)
#rm /mnt/boot/refind_linux.conf
#tee -a /mnt/boot/refind_linux.conf <<EOF
#"Boot using default options" "root=PARTUUID=$DISK_UUID rw add_efi_memmap initrd=boot\intel-ucode.img initrd=boot\initramfs-linux.img"
#EOF
printf "\e[1;32mDone! Type exit, umount -a and reboot.\e[0m"
